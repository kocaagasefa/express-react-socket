const Product = require("../models/product"),
  Category = require("../models/category");

module.exports = (app, io) => {
  app.get("/api/products", (req, res) => {
    Product.find({})
      .populate("category")
      .then(products => res.json(products));
  });

  app.post("/api/productManagement", (req, res) => {
    const { productName, productImage, categoryId, price, quantity } = req.body;
    Product.create({
      name: productName,
      image: productImage,
      price,
      quantity,
      category: { _id: categoryId }
    })
      .then(product => product.populate("category").execPopulate())
      .then(product => {
        io.sockets.emit("change", { action: "addProduct", data: product });
        res.send(product);
      });
  });

  app.delete("/api/productManagement", (req, res) => {
    const _id = req.body.productId;
    Product.findByIdAndDelete(_id).then(product => {
      io.sockets.emit("change", { action: "deleteProduct", data: product });
      res.json(product);
    });
  });

  app.put("/api/productManagement", (req, res) => {
    const _id = req.body.productId;
    Product.findOne({ _id })
      .then(product => {
        product.name = req.body.productName;
        product.image = req.body.productImage;
        product.price = req.body.price;
        product.quantity = req.body.quantity;
        product.category = req.body.categoryId;
        return product.save();
      })
      .then(product => product.populate("category").execPopulate())
      .then(product => {
        io.sockets.emit("change", { action: "updateProduct", data: product });
        res.json(product);
      });
  });
  app.get("/api/categories", (req, res) => {
    Category.find({}).then(products => res.json(products));
  });

  app.post("/api/categoryManagement", (req, res) => {
    const { categoryName } = req.body;
    Category.create({
      name: categoryName
    }).then(category => res.send(category));
  });

  app.delete("/api/categoryManagement", (req, res) => {
    const _id = req.body.categoryId;
    Category.findByIdAndDelete(_id).then(category => {
      Product.deleteMany({ category: _id }).then(result => {
        io.sockets.emit("change", { action: "deleteCategory", data: category });
        res.json(category);
      });
    });
  });

  app.put("/api/categoryManagement", (req, res) => {
    const _id = req.body.categoryId;

    Category.findOne({ _id })
      .then(category => {
        category.name = req.body.categoryName;
        return category.save();
      })
      .then(category => {
        io.sockets.emit("change", { action: "updateCategory", data: category });
        res.json(category);
      });
  });
};

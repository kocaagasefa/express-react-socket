const mongoose = require("mongoose");
const AutoIncrement = require("mongoose-sequence")(mongoose);

const productSchema = mongoose.Schema(
  {
    _id: Number,
    category: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Category"
    },
    name: String,
    image: String,
    price:Number,
    quantity:Number
  },
  { _id: false }
);
productSchema.plugin(AutoIncrement);

module.exports = mongoose.model("Product", productSchema);

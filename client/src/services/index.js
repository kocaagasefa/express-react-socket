import Axios from "axios";
import { apiURL } from "../constants";

export const getProducts = callback =>
  Axios.get(apiURL + "/products")
    .then(response => {
      callback && callback();
      return response.data;
    })
    .catch(error => {
      console.log(error);
      callback && callback();
      return false;
    });

export const addProduct = (product, callback) =>
  Axios.post(apiURL + "/productManagement", product)
    .then(response => {
      callback && callback();
      return response.data;
    })
    .catch(error => {
      console.log(error);
      callback && callback();
      return false;
    });

export const deleteProduct = (productId, callback) =>
  Axios.delete(apiURL + "/productManagement", { data: { productId } })
    .then(response => {
      callback && callback();
      return true;
    })
    .catch(error => {
      console.log(error);
      callback && callback();
      return false;
    });

export const updateProduct = (params, callback) =>
  Axios.put(apiURL + "/productManagement", params)
    .then(response => {
      callback && callback();
      return response.data;
    })
    .catch(error => {
      console.log(error);
      callback && callback();
      return false;
    });

export const getCategories = callback =>
  Axios.get(apiURL + "/categories")
    .then(response => {
      callback && callback();
      return response.data;
    })
    .catch(error => {
      console.log(error);
      callback && callback();
      return false;
    });

export const addCategory = (categoryName, callback) =>
  Axios.post(apiURL + "/categoryManagement", { categoryName })
    .then(response => {
      callback && callback();
      return response.data;
    })
    .catch(error => {
      console.log(error);
      callback && callback();
      return false;
    });

export const deleteCategory = (categoryId, callback) =>
  Axios.delete(apiURL + "/categoryManagement", { data: { categoryId } })
    .then(response => {
      callback && callback();
      return response.data;
    })
    .catch(error => {
      console.log(error);
      callback && callback();
      return false;
    });

export const updateCategory = (params, callback) =>
  Axios.put(apiURL + "/categoryManagement", params)
    .then(response => {
      callback && callback();
      return response.data;
    })
    .catch(error => {
      console.log(error);
      callback && callback();
      return false;
    });

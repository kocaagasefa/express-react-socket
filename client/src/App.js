import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Products from "./pages/products/products";
import "semantic-ui-css/semantic.min.css";
import "./App.css";
import Layout from "./components/layout/layout";
import Manage from "./pages/manage";

function App() {
  return (
    <BrowserRouter>
      <Layout>
        <Switch>
          <Route path="/manage" component={Manage} />
          <Route exact path="/" component={Products} />
        </Switch>
      </Layout>
    </BrowserRouter>
  );
}

export default App;

export const updateProducts = (products, change) => {
  switch (change.action) {
    case "updateProduct":
      return products.map(product =>
        product._id === change.data._id ? change.data : product
      );
    case "deleteProduct":
      return products.filter(product => product._id !== change.data._id);
    case "addProduct":
      return [...products, change.data];
    case "deleteCategory":
        return products.filter(product => product.category && product.category._id !== change.data._id)
    case "updateCategory":
      return products.map(product =>
        product.category && product.category._id === change.data._id
          ? {
              ...product,
              category: change.data
            }
          : product
      );
    default:
      return products;
  }
};

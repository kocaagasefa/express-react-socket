import React, { useEffect, useState, useCallback } from "react";
import io from "socket.io-client";
import { socketURL } from "../../constants";
import {
  Grid,
  Form,
  Button,
  Segment,
  Header,
  Container
} from "semantic-ui-react";
import ProductCard from "../../components/ProductCard/ProductCard";
import { getProducts, getCategories } from "../../services";
import { updateProducts } from "./updateProducts";

const Products = () => {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [showCategorySelect, setShowCategorySelect] = useState(true);
  const [categories, setCategories] = useState([]);
  const [selectedCategories, setSelectedCategories] = useState([]);

  useEffect(() => {
    setLoading(true);
    getCategories(() => setLoading(false)).then(
      categories => categories && setCategories(categories)
    );
  }, []);

  const getSelectedProducts = useCallback(() => {
    setLoading(true);
    getProducts(() => setLoading(false)).then(
      products => products && setProducts(products)
    );
  }, []);

  const isSelected = _id => selectedCategories.includes(_id);
  useEffect(() => {
    const socket = io(socketURL);
    socket.on("change", change => {
      const newProducts = updateProducts(products, change);
      setProducts(newProducts);
    });
    return () => socket.disconnect();
  }, [products]);

  const toggleCategorySelect = _id => {
    setSelectedCategories(
      isSelected(_id)
        ? selectedCategories.filter(categoryId => categoryId !== _id)
        : [...selectedCategories, _id]
    );
  };
  const getSelectedCategories = () => {
    getSelectedProducts();
    setShowCategorySelect(false);
  };
  const SelectCategory = () => (
    <Container>
      <Segment padded>
        <Header>Takip etmek istediğiniz kategorileri seçiniz:</Header>
        <Form
          loading={loading}
          style={{ paddingBottom: "2em" }}
          onSubmit={getSelectedCategories}
        >
          {categories.map(category => (
            <Form.Checkbox
              fitted
              label={category.name}
              key={category._id}
              checked={isSelected(category._id)}
              onChange={() => toggleCategorySelect(category._id)}
            />
          ))}
          <Button
            type="submit"
            floated="right"
            color="blue"
            disabled={!selectedCategories[0]}
          >
            Onayla
          </Button>
        </Form>
      </Segment>
    </Container>
  );
  return showCategorySelect ? (
    <SelectCategory />
  ) : (
    <>
      <Grid container columns="3" centered stackable>
        {products
          .filter(
            product =>
              product.category &&
              selectedCategories.includes(product.category._id)
          )
          .map(product => (
            <Grid.Column key={product._id}>
              <ProductCard {...product} />
            </Grid.Column>
          ))}
      </Grid>
    </>
  );
};

export default Products;

import React from 'react';
import { Switch, Route } from 'react-router-dom';
import ProductManagement from './productManagement';
import CategoryManagement from './categoryManagement';
const Manage = () => (
    <Switch>
        <Route path="/manage/products" exact  component={ProductManagement} />
        <Route path="/manage/categories" exact  component={CategoryManagement} />
    </Switch>
);

export default Manage;
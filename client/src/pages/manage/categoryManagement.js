import React, { useEffect, useState, useCallback } from "react";
import {
  Container,
  Segment,
  Input,
  Icon,
  Modal,
  Button,
  Table
} from "semantic-ui-react";
import { getCategories, addCategory, deleteCategory, updateCategory } from "../../services";
import classNames from "./style.module.css";

const CategoryManagement = () => {
  const [loadingCategories, setLoadingCategories] = useState(false);
  const [categories, setCategories] = useState([]);
  const [categoryName, setCategoryName] = useState("");
  const [deleteCategoryId, setDeleteCategoryId] = useState(-1);
  const [updateCategoryId, setUpdateCategoryId] = useState(-1);
  const [newCategoryName, setNewCategoryName] = useState("");
  useEffect(() => {
    setLoadingCategories(true);
    getCategories(() => {
      setLoadingCategories(false);
    }).then(categories => categories && setCategories(categories));
  }, []);

  const deleteCategoryHandler = useCallback(() => {
    deleteCategory(deleteCategoryId).then(res => {
      res &&
        setCategories(
          categories.filter(category => category._id !== deleteCategoryId)
        );
      setDeleteCategoryId(-1);
    });
  }, [categories, deleteCategoryId]);

  const updateCategoryHandler = useCallback(
    () => {
      const params={
        categoryId:updateCategoryId,
        categoryName:newCategoryName
      }
      updateCategory(params).then(updatedCategory => {
        updatedCategory &&
          setCategories(
            categories.map(category =>
              category._id === updatedCategory._id ? updatedCategory : category
            )
          );
        setUpdateCategoryId(-1);
        setNewCategoryName("");
      });
    },
    [categories, newCategoryName, updateCategoryId]
  );

  const addCategoryHandler = useCallback(() => {
    addCategory(categoryName).then(
      category => category && setCategories([...categories, category])
    );
    setCategoryName("");
  }, [categories, categoryName]);
  return (
    <>
      <Container as={Segment} loading={loadingCategories}>
        <Table>
          <Table.Header>
            <Table.Row>
            <Table.HeaderCell>
                <Input
                  fluid
                  action={{
                    color: "blue",
                    labelPosition: "right",
                    icon: "add",
                    content: "Add",
                    onClick: addCategoryHandler,
                    disabled: !categoryName.trim()
                  }}
                  value={categoryName}
                  onChange={(e, data) => setCategoryName(data.value)}
                />
              </Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>

            {categories.map(category => (
              <Table.Row key={category._id} className={classNames.listItem}>
                <Table.Cell>
                  {category._id === updateCategoryId ? (
                    <>
                      <Input
                        value={newCategoryName}
                        onChange={(e, data) => setNewCategoryName(data.value)}
                      />
                      <Button color="red" onClick={()=>{
                        setNewCategoryName("");
                        setUpdateCategoryId(-1);
                      }}>İptal</Button>
                      <Button color="blue" onClick={updateCategoryHandler}>Güncelle</Button>
                    </>
                  ) : (
                    category.name
                  )}

                  <Icon
                    className={classNames.deleteIcon}
                    name="delete"
                    color="red"
                    onClick={() => {
                      setDeleteCategoryId(category._id);
                    }}
                    bordered
                  />
                  <Icon
                    className={classNames.editIcon}
                    name="edit"
                    color="teal"
                    onClick={() => {
                      setUpdateCategoryId(category._id);
                      setNewCategoryName(category.name);
                    }}
                    bordered
                  />
                </Table.Cell>
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
      </Container>
      <Modal
        dimmer="inverted"
        header="Dikkat!"
        open={deleteCategoryId !== -1}
        onClose={() => setDeleteCategoryId(-1)}
        content="Bu kategoriye ait tüm ürünler de silinecek. Onaylıyor musunuz?"
        actions={[
          "İptal",
          {
            key: "done",
            content: "Sil",
            negative: true,
            onClick: deleteCategoryHandler
          }
        ]}
      />
    </>
  );
};

export default CategoryManagement;

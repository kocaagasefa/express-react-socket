import React, { useEffect, useState, useCallback } from "react";
import {
  Container,
  Table,
  Image,
  Segment,
  Icon,
  Button,
  Modal
} from "semantic-ui-react";
import {
  getProducts,
  deleteProduct,
  updateProduct,
  addProduct
} from "../../services";
import classNames from "./style.module.css";
import AddOrUpdateProduct from "../../components/AddOrUpdateProduct/AddOrUpdateProduct";

const ProductManagement = () => {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [addOrUpdateLoading, setAddOrUpdateLoading] = useState(false);
  const [addModal, setAddModal] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState();

  useEffect(() => {
    setLoading(true);
    getProducts(() => setLoading(false)).then(
      data => data && setProducts(data)
    );
  }, []);
  const deleteProductHandler = useCallback(
    _id => {
      setLoading(true);
      deleteProduct(_id, () => setLoading(false)).then(res => {
        if (res) setProducts(products.filter(product => product._id !== _id));
      });
    },
    [products]
  );
  const updateProductHandler = useCallback(
    params => {
      setAddOrUpdateLoading(true);
      updateProduct(params,()=> setAddOrUpdateLoading(false)).then(updatedProduct => {
        if (updatedProduct)
          setProducts(
            products.map(product =>
              product._id === updatedProduct._id ? updatedProduct : product
            )
          );
        setSelectedProduct();
      });
    },
    [products]
  );
  const addNewProductHandler = useCallback(
    params => {
      setAddOrUpdateLoading(true)
      addProduct(params,()=> setAddOrUpdateLoading(false)).then(newProduct => {
        if (newProduct) setProducts([...products, newProduct]);
        setAddModal();
      });
    },
    [products]
  );
  return (
    <>
      <Container as={Segment} loading={loading}>
        <Table selectable>
          <Table.Header fullWidth>
            <Table.Row>
              <Table.HeaderCell colSpan="6">
                <Button
                  floated="right"
                  icon
                  labelPosition="left"
                  color="blue"
                  size="small"
                  onClick={() => setAddModal(true)}
                >
                  <Icon name="add" /> Yeni Ekle
                </Button>
              </Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Fotoğraf</Table.HeaderCell>
              <Table.HeaderCell>Ürün No</Table.HeaderCell>
              <Table.HeaderCell>Ürün Adı</Table.HeaderCell>
              <Table.HeaderCell>Kategori</Table.HeaderCell>
              <Table.HeaderCell>Fiyat</Table.HeaderCell>
              <Table.HeaderCell>Miktar</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body className={classNames.productTable}>
            {products.map(product => (
              <Table.Row key={product._id}>
                <Table.Cell>
                  <Image src={product.image}  rounded size="mini" />
                </Table.Cell>
                <Table.Cell>{product._id}</Table.Cell>
                <Table.Cell>{product.name}</Table.Cell>
                <Table.Cell>
                  {product.category && product.category.name}
                </Table.Cell>
                <Table.Cell>{product.price}</Table.Cell>
                <Table.Cell>
                  {product.quantity}
                  <Icon
                    className={classNames.deleteIcon}
                    name="delete"
                    color="red"
                    onClick={() => deleteProductHandler(product._id)}
                    bordered
                  />
                  <Icon
                    className={classNames.editIcon}
                    name="edit"
                    color="teal"
                    onClick={() => setSelectedProduct(product)}
                    bordered
                  />
                </Table.Cell>
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
      </Container>
      <Modal
        dimmer="inverted"
        open={addModal || !!selectedProduct}
        onClose={() => {
          setAddModal(false);
          setSelectedProduct();
        }}
      >
        <Modal.Header>
          {selectedProduct ? "Ürün Güncelle" : "Yeni Ürün Ekle"}
        </Modal.Header>
        <Modal.Content>
          <AddOrUpdateProduct
            product={selectedProduct}
            onLoading={addOrUpdateLoading}
            onSubmit={
              selectedProduct ? updateProductHandler : addNewProductHandler
            }
          />
        </Modal.Content>
      </Modal>
    </>
  );
};

export default ProductManagement;

import React from "react";
import { NavLink } from "react-router-dom";
import { Menu, Container, Dropdown, Icon } from "semantic-ui-react";

const Header = () => (
  <Menu fixed="top" size="massive" color="blue" inverted>
    <Container>
      <Menu.Item as={NavLink} to="/" exact>
        <Icon name="home" />
        Ana Sayfa
      </Menu.Item>
      <Menu.Menu position="right">
        <Menu.Item as={Dropdown} text="Yönet">
          <Dropdown.Menu>
            <Dropdown.Item as={NavLink} to="/manage/products">
              Ürünler
            </Dropdown.Item>
            <Dropdown.Item as={NavLink} to="/manage/categories">
              Kategoriler
            </Dropdown.Item>
          </Dropdown.Menu>
        </Menu.Item>
      </Menu.Menu>
    </Container>
  </Menu>
);

export default Header;

import React from "react";
import Header from "../header/header";

const Layout = ({ children }) => (
  <>
    <Header />
    <main style={{paddingTop:75, minHeight:"100vh",background:"linear-gradient(120deg, #fdfbfb 0%, #ebedee 100%)"}}>{children}</main>
  </>
);

export default Layout;

import React from "react";
import { Card, Image } from "semantic-ui-react";
import classNames from './style.module.css';
const ProductCard = ({ name, image, quantity, price, category }) => (
  <Card centered>      
    <Image src={image} 
    className={classNames.image}
    label={{color:"blue", content:price+" TL", ribbon:true, size:"big"}}
    />
    <Card.Content>
      <Card.Header>{name}</Card.Header>
      <Card.Meta>
        <span>{category && category.name}</span>
      </Card.Meta>
    </Card.Content>
    <Card.Content extra>
        <span>Son <b>{quantity} adet</b></span>
    </Card.Content>
  </Card>
);

export default ProductCard;

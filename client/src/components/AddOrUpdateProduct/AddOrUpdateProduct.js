import React, { useState, useEffect } from "react";
import { getCategories } from "../../services";
import { Form, Input, Grid, Image, Button, Select } from "semantic-ui-react";

const AddOrUpdateProduct = ({
  product = {
    _id: "",
    category: { _id: "" },
    name: "",
    image: "",
    quantity: 0,
    price: 0
  },
  onSubmit,
  onLoading
}) => {
  const [loadingCategories, setLoadingCategories] = useState(false);
  const [categories, setCategories] = useState([]);
  const [params, setParams] = useState({
    productId: product._id,
    productName: product.name,
    productImage: product.image,
    quantity: product.quantity,
    price: product.price,
    categoryId: product.category._id
  });
  const inputChangehandler = (e, data) => {
      const {name, value} = data;
      setParams({
          ...params,
          [name]:value
      })
  }
  useEffect(() => {
    setLoadingCategories(true);
    getCategories(() => setLoadingCategories(false)).then(
      categories => categories && setCategories(categories)
    );
  }, []);
  const isDisabled = () => !(params.productName.trim() && params.productImage.trim() && params.categoryId) 
  return (
    <Grid stackable columns={2} verticalAlign="middle">
      <Grid.Column width={4}>
        <Image
          src={
            params.productImage ||
            "https://react.semantic-ui.com/images/wireframe/image.png"
          }
        />
      </Grid.Column>
      <Grid.Column width={12}>
        <Form onSubmit={()=> onSubmit(params)} loading={onLoading}>
          <Form.Field
            label="Ürün Adı"
            control={Input}
            value={params.productName}
            name="productName"
            onChange={inputChangehandler}
          />
          <Form.Field
            label="Resim"
            control={Input}
            value={params.productImage}
            name="productImage"
            onChange={inputChangehandler}
          />
          <Form.Field
            label="Kategori"
            control={Select}
            loading={loadingCategories}
            value={params.categoryId}
            name="categoryId"
            onChange={inputChangehandler}
            options={categories.map(category => ({
              key: category._id,
              text: category.name,
              value: category._id
            }))}
          />
          <Form.Field
            label="Stok Adedi"
            control={Input}
            type="number"
            value={params.quantity}
            name="quantity"
            onChange={inputChangehandler}
          />
          <Form.Field
            label="Fiyat"
            control={Input}
            type="number"
            value={params.price}
            name="price"
            onChange={inputChangehandler}
          />
          <Button type="submit" floated="right" color="blue" disabled={isDisabled()}>
            Onayla
          </Button>
        </Form>
      </Grid.Column>
    </Grid>
  );
};

export default AddOrUpdateProduct;

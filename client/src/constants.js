export const apiURL =
  process.env.NODE_ENV === "production" ? "/api" : "http://localhost:3000/api";

export const socketURL =
  process.env.NODE_ENV === "production" ? "/" : "http://localhost:3000";

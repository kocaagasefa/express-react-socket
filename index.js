const express = require("express");
const cors = require("cors");
const path = require("path");
const http = require("http");
const socketIo = require("socket.io");
const mongoose = require("mongoose");

const connection_string =
  "mongodb://admin:12345678abc@ds259367.mlab.com:59367/egemsoft";
const port = process.env.PORT || 3000;
mongoose.set("useCreateIndex", true);

mongoose.connect(connection_string, { useNewUrlParser: true });
mongoose.set("useFindAndModify", false);
const app = express();
app.use(express.static(path.join(__dirname, "client/build")));
app.use(cors());
app.use(express.json());

const server = http.createServer(app);
const io = socketIo(server);
io.on("connection", socket => {
  console.log("a user connected");
  io.on("disconnect", socket => {
    console.log("closing");
    socket.close();
  });
});
require("./api")(app, io);

app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname + "/client/build/index.html"));
});

server.listen(port, () => console.log("listening on port:", port));
